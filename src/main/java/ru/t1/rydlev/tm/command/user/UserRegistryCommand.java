package ru.t1.rydlev.tm.command.user;

import ru.t1.rydlev.tm.api.service.IAuthService;
import ru.t1.rydlev.tm.model.User;
import ru.t1.rydlev.tm.util.TerminalUtil;

public final class UserRegistryCommand extends AbstractUserCommand {

    @Override
    public void execute() {
        System.out.println("[USER REGISTRY]");
        System.out.println("ENTER LOGIN:");
        final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD:");
        final String password = TerminalUtil.nextLine();
        System.out.println("ENTER EMAIL:");
        final String email = TerminalUtil.nextLine();
        final IAuthService authService = serviceLocator.getAuthService();
        final User user = authService.registry(login, password, email);
        showUser(user);
    }

    @Override
    public String getDescription() {
        return "Registry user.";
    }

    @Override
    public String getName() {
        return "user-registry";
    }

}
