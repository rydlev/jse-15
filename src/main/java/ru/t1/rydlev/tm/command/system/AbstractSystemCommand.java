package ru.t1.rydlev.tm.command.system;

import ru.t1.rydlev.tm.api.service.ICommandService;
import ru.t1.rydlev.tm.command.AbstractCommand;

public abstract class AbstractSystemCommand extends AbstractCommand {

    protected ICommandService getCommandService() {
        return serviceLocator.getCommandService();
    }

}
