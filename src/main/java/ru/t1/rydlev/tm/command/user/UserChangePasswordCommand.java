package ru.t1.rydlev.tm.command.user;

import ru.t1.rydlev.tm.util.TerminalUtil;

public final class UserChangePasswordCommand extends AbstractUserCommand {

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[USER CHANGE PASSWORD]");
        System.out.println("ENTER PASSWORD:");
        final String password = TerminalUtil.nextLine();
        getUserService().setPassword(userId, password);
    }

    @Override
    public String getDescription() {
        return "Change password of current user.";
    }

    @Override
    public String getName() {
        return "change-user-password";
    }

}
