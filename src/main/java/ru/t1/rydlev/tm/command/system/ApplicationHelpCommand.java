package ru.t1.rydlev.tm.command.system;

import ru.t1.rydlev.tm.api.model.ICommand;
import ru.t1.rydlev.tm.command.AbstractCommand;

import java.util.Collection;

public final class ApplicationHelpCommand extends AbstractSystemCommand {

    @Override
    public void execute() {
        System.out.println("[HELP]");
        final Collection<AbstractCommand> commands = getCommandService().getTerminalCommands();
        for (final ICommand command : commands) System.out.println(command);
    }

    @Override
    public String getArgument() {
        return "-h";
    }

    @Override
    public String getDescription() {
        return "Show application commands.";
    }

    @Override
    public String getName() {
        return "help";
    }

}
