package ru.t1.rydlev.tm.api.repository;

import ru.t1.rydlev.tm.model.Project;

import java.util.Comparator;
import java.util.List;

public interface IProjectRepository {

    boolean existsById(String id);

    List<Project> findAll();

    List<Project> findAll(Comparator<Project> comparator);

    Project add(Project project);

    void deleteAll();

    Project findOneById(String id);

    Project findOneByIndex(Integer index);

    Project remove(Project project);

    Project removeById(String id);

    Project removeByIndex(Integer index);

}
